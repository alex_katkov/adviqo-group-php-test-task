<?php
declare(strict_types=1);

namespace App\Tests\Validator;

use App\Validator\CurrencyConstraint;
use App\Validator\CurrencyConstraintValidator;
use Generator;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvokedCount;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class CurrencyConstraintValidatorTest extends TestCase
{
    private CurrencyConstraintValidator $validator;
    private MockObject|ExecutionContextInterface $executionContext;
    private MockObject|ConstraintViolationBuilderInterface $constraintViolationBuilder;
    private MockObject|CurrencyConstraint $constraint;

    protected function setUp(): void
    {
        $this->validator = new CurrencyConstraintValidator();
        $this->executionContext = $this->createMock(ExecutionContextInterface::class);
        $this->constraintViolationBuilder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $this->constraint = $this->createMock(CurrencyConstraint::class);
    }

    /** @dataProvider validateSkipDataGenerator */
    public function testValidateSkip(mixed $value): void
    {
        $this->executionContext
            ->expects(self::never())
            ->method('getObject');

        $this->validator->initialize($this->executionContext);
        $this->validator->validate($value, $this->constraint);
    }

    public function validateSkipDataGenerator(): Generator
    {
        yield ['value' => null];
        yield ['value' => ''];
    }

    /** @dataProvider validateDataGenerator */
    public function testValidate(
        Money $value,
        InvokedCount $expectedViolationCount,
        InvokedCount $expectedBuilderCount,
        InvokedCOunt $expectedParameterCount
    ): void {
        $this->executionContext
            ->expects($expectedViolationCount)
            ->method('buildViolation')
            ->willReturn($this->constraintViolationBuilder);

        $this->constraintViolationBuilder
            ->expects($expectedParameterCount)
            ->method('setParameter')
            ->willReturn($this->constraintViolationBuilder);

        $this->constraintViolationBuilder
            ->expects($expectedBuilderCount)
            ->method('addViolation');

        $this->validator->initialize($this->executionContext);
        $this->validator->validate($value, $this->constraint);
    }

    public function validateDataGenerator(): Generator
    {
        // Valid currency code
        yield [
            'value' => new Money(25, new Currency('EUR')),
            'expectedViolationCount' => self::never(),
            'expectedBuilderCount' => self::never(),
            'expectedParameterCount' => self::never(),
        ];

        // Invalid currency code
        yield [
            'value' => new Money(25, new Currency('UUU')),
            'expectedViolationCount' => self::once(),
            'expectedBuilderCount' => self::once(),
            'expectedParameterCount' => self::once(),
        ];
    }
}
