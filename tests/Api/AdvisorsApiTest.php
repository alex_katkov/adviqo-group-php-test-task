<?php
declare(strict_types=1);

namespace App\Tests\Api;

use App\Entity\Advisor;
use App\Tests\AliceFixtureDependentTestCase;
use App\Tests\Traits\DatabasePurger;
use App\Tests\Traits\GetContainerInstance;
use App\Tests\Traits\GetEntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class AdvisorsApiTest extends AliceFixtureDependentTestCase
{
    use DatabasePurger, GetContainerInstance, GetEntityManager;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();

        self::ensureKernelShutdown();
        $this->client = self::createClient();
    }

    public function testGetCollection(): void
    {
        $this->client->request('GET', '/api/advisors');

        self::assertResponseIsSuccessful();

        /** @var Advisor $firstAdvisorRecordReference */
        $firstAdvisorRecordReference = $this->getReference('advisor_1');
        $response = $this->client->getResponse();

        self::assertStringContainsString(
            $firstAdvisorRecordReference->getId()->jsonSerialize(),
            $response->getContent()
        );
    }

    public function testGetItem(): void
    {
        /** @var Advisor $firstAdvisorRecordReference */
        $firstAdvisorRecordReference = $this->getReference('advisor_1');
        $uid = $firstAdvisorRecordReference->getId()->jsonSerialize();

        $this->client->request('GET', '/api/advisors/' . $uid);
        self::assertResponseIsSuccessful();

        $response = $this->client->getResponse();

        self::assertStringContainsString(
            $uid,
            $response->getContent()
        );
    }

    public function testRemoveItem(): void
    {
        /** @var Advisor $firstAdvisorRecordReference */
        $firstAdvisorRecordReference = $this->getReference('advisor_1');
        $uid = $firstAdvisorRecordReference->getId()->jsonSerialize();

        $this->client->request('DELETE', '/api/advisors/' . $uid);
        self::assertResponseIsSuccessful();

        $repository = $this->entityManager->getRepository(Advisor::class);
        self::assertNull($repository->find($uid));
    }

    public function testPostItem(): void
    {
        $server = [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json',
            'HTTP_ACCEPT_LANGUAGE' => 'en',
        ];

        $payload = [
            'name' => 'Post Record Name',
            'description' => '',
            'pricePerMinute' => [
                'amount' => '333',
                'currency' => 'USD',
            ],
            'locales' => ['en', 'fr'],
        ];


        $this->client->request(
            'POST',
            '/api/advisors',
            [],
            [],
            $server,
            json_encode($payload)
        );

        $response = $this->client->getResponse();
        self::assertResponseIsSuccessful();

        $repository = $this->entityManager->getRepository(Advisor::class);
        self::assertNotNull($repository->findOneBy(['name' => $payload['name']]));
    }

    protected function getFixtureFiles(): array
    {
        return ['Advisor.yml'];
    }
}
