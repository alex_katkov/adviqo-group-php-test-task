DC=docker-compose
DOCKER=docker
APP=app
env=dev
src=assets
RUN=$(DC) run --rm $(APP)
EXEC=$(DC) exec
APP-EXEC=$(EXEC) --user=a1ex-k $(APP)
WORKER-EXEC=$(EXEC) --user=a1ex-k worker
CONSOLE=bin/console

install:
	@$(DC) up -d --build --remove-orphans
	make app-composer-install
	make app-db-create

up:
	@$(DC) up -d --remove-orphans

build:
	@$(DC) build --parallel --no-cache --compress --force-rm --pull

wait:
	@echo "Waiting for container to be healthy"
	@$(RUN) sleep 1m

down:
	@$(DC) down -v --rmi all --remove-orphans

remove:
	make down
	make postgres-volume-remove
	make redis-volume-remove
	make rabbitmq-volume-remove

stop:
	@$(DC) stop

restart:
	@$(DC) restart

app-sh:
	$(APP-EXEC) sh

app-composer-install:
	$(APP-EXEC) composer install --no-scripts --no-suggest -o

app-composer-remove:
	$(APP-EXEC) composer remove $(package)

app-composer-require:
	$(APP-EXEC) composer require $(package)

app-composer-require-dev:
	$(APP-EXEC) composer require --dev $(package)

app-db-create:
	$(APP-EXEC) php bin/console doctrine:database:create --if-not-exists --env=test

app-db-create-test:
	$(APP-EXEC) php bin/console doctrine:database:create --if-not-exists --env=$(env)

app-migration-diff:
	$(APP-EXEC) php bin/console doctrine:cache:clear-metadata
	$(APP-EXEC) php bin/console doctrine:cache:clear-query
	$(APP-EXEC) php bin/console doctrine:migrations:diff -n

app-migration-migrate:
	$(APP-EXEC) php bin/console doctrine:migrations:migrate -n --env=$(env) --allow-no-migration

app-fixtures-load:
	$(APP-EXEC) php bin/console doctrine:fixtures:load --env=$(env) -n

app-db-remove:
	$(APP-EXEC) php bin/console doctrine:database:drop --env=$(env) --force --if-exists

app-schema-create:
	$(APP-EXEC) php bin/console doctrine:schema:create --env=$(env)

app-schema-remove:
	$(APP-EXEC) php bin/console doctrine:schema:drop --env=$(env) --force --full-database

app-schema-validate:
	$(APP-EXEC) php bin/console doctrine:schema:validate --env=$(env)

app-cache-clear:
	$(APP-EXEC) rm -rf var/cache
	$(APP-EXEC) composer dumpautoload
	$(APP-EXEC) php bin/console cache:clear --env=$(env)
	$(APP-EXEC) php bin/console doctrine:cache:clear-metadata
	$(APP-EXEC) php bin/console doctrine:cache:clear-query

app-cache-clear-test:
	$(APP-EXEC) rm -rf var/cache
	$(APP-EXEC) php bin/console cache:clear --env=test
	$(APP-EXEC) php bin/console doctrine:cache:clear-metadata --env=test
	$(APP-EXEC) php bin/console doctrine:cache:clear-query --env=test

app-lint:
	make app-lint-phpstan
	make app-lint-phpcs
	make app-lint-parallel

app-lint-phpstan:
	$(APP-EXEC) vendor/bin/phpstan clear-result-cache
	$(APP-EXEC) vendor/bin/phpstan analyse -l 6 -c phpstan.neon --no-progress --memory-limit=512M --error-format=table src tests

app-lint-parallel:
	$(APP-EXEC) vendor/bin/parallel-lint --exclude ./bin/.phpunit --exclude vendor .

app-lint-phpcs:
	$(APP-EXEC) vendor/bin/phpcs --standard=phpcs.xml.dist

app-phpunit:
	$(APP-EXEC) bin/phpunit tests --stop-on-failure

app-test:
	make app-db-remove env=test
	make app-db-create env=test
	make app-migration-migrate env=test
	make app-phpunit

nginx-sh:
	$(EXEC) nginx sh

postgres-sh:
	$(EXEC) postgres sh
