<?php
declare(strict_types=1);

namespace App\Dto;

final class AdvisorInputDto
{
    public ?string $name;
    public ?string $description;
    public bool $available = false;
    public array $pricePerMinute = [];
    public array $locales = [];
}
