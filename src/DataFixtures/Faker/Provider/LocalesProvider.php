<?php
declare(strict_types=1);

namespace App\DataFixtures\Faker\Provider;

use Symfony\Component\Intl\Languages;

class LocalesProvider
{
    public function getRandomLocales(int $count = 1): array
    {
        $locales = $this->getMainLocales();
        $randomArrayKeys = array_rand($this->getMainLocales(), $count);

        return (1 === $count)
            ? [$locales[$randomArrayKeys]]
            : array_intersect_key($locales, array_flip($randomArrayKeys));
    }

    private function getMainLocales(): array
    {
        return array_filter(
            Languages::getLanguageCodes(),
            static fn(string $key) => 2 === strlen($key)
        );
    }
}
