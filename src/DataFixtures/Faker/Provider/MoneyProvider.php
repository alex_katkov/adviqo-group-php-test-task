<?php
declare(strict_types=1);

namespace App\DataFixtures\Faker\Provider;

use Money\Money;

class MoneyProvider
{
    public function getMoneyFixture(int|float|string $amount, string $currency = 'EUR'): Money
    {
        return Money::$currency($amount);
    }
}
