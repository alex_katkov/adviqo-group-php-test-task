<?php
declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\AdvisorInputDto;
use App\Entity\Advisor;
use Money\Currency;
use Money\Money;

final class AdvisorInputTransformer implements DataTransformerInterface
{
    /** @var AdvisorInputDto $AdvisorInputDto */
    public function transform($AdvisorInputDto, string $to, array $context = []): Advisor
    {
        $advisor = new Advisor();

        $advisor
            ->setName($AdvisorInputDto->name)
            ->setDescription($AdvisorInputDto->description)
            ->setAvailable($AdvisorInputDto->available)
            ->setPricePerMinute(new Money(
                $AdvisorInputDto->pricePerMinute['amount'],
                new Currency($AdvisorInputDto->pricePerMinute['currency'])
            ));

        foreach ($AdvisorInputDto->locales as $locale) {
            $advisor->addLocale($locale);
        }

        return $advisor;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return Advisor::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
