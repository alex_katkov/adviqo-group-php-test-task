<?php
declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class MoneyValuePositiveConstraint extends Constraint
{
    public string $message = 'money.value_positive';
}
