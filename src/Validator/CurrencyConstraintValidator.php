<?php
declare(strict_types=1);

namespace App\Validator;

use Money\Currencies\ISOCurrencies;
use Money\Money;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class CurrencyConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof CurrencyConstraint) {
            throw new UnexpectedTypeException($constraint, CurrencyConstraint::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!$value instanceof Money) {
            throw new UnexpectedValueException($value, Money::class);
        }

        $currency = $value->getCurrency();

        if (!(new ISOCurrencies())->contains($currency)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ currency }}', $currency->getCode())
                ->addViolation();
        }
    }
}
