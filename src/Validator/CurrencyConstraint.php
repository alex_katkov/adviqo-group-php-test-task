<?php
declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class CurrencyConstraint extends Constraint
{
    public string $message = 'money.currency';
}
