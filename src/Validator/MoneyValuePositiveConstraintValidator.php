<?php
declare(strict_types=1);

namespace App\Validator;

use Money\Money;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class MoneyValuePositiveConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof MoneyValuePositiveConstraint) {
            throw new UnexpectedTypeException($constraint, MoneyValuePositiveConstraint::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!$value instanceof Money) {
            throw new UnexpectedValueException($value, Money::class);
        }

        if ($value->isNegative()) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
