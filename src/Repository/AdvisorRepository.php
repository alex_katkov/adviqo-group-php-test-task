<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Advisor;
use Doctrine\Persistence\ManagerRegistry;

class AdvisorRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advisor::class);
    }
}
