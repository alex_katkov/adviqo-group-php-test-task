<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\Persistence\ManagerRegistry;

abstract class BaseRepository extends ServiceEntityRepository
{
    protected string $entityClass;

    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        $this->entityClass = $entityClass;

        parent::__construct($registry, $entityClass);
    }

    public function persist(object $entity, bool $flush = false): void
    {
        if (!$this->canHandleEntity($entity)) {
            throw ORMInvalidArgumentException::invalidObject(get_class($this), $entity);
        }

        $this->_em->persist($entity);

        if ($flush) {
            $this->flush();
        }
    }

    public function remove(object $entity, bool $flush = false): void
    {
        if (!$entity instanceof $this->entityClass) {
            throw ORMInvalidArgumentException::invalidObject(get_class($this), $entity);
        }

        $this->_em->remove($entity);

        if ($flush) {
            $this->flush();
        }
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    public function refresh(object $entity): void
    {
        $this->_em->refresh($entity);
    }

    public function canHandleEntity(object $object): bool
    {
        return $object instanceof $this->entityClass;
    }

    public function supports(string $className): bool
    {
        return $className === $this->entityClass;
    }
}
