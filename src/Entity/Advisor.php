<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\AdvisorInputDto;
use App\Entity\Traits\Id;
use App\Repository\AdvisorRepository;
use App\Validator as AppAssert;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AdvisorRepository::class)]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'put', 'delete'],
    input: AdvisorInputDto::class,
)]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'partial', 'locales' => 'partial'])]
#[ApiFilter(
    OrderFilter::class,
    properties: ['available'],
    arguments: ['orderParameterName' => 'order']
)]
class Advisor
{
    use Id;

    #[ORM\Column(name: 'name', type: 'string', length: 80)]
    #[Assert\Length(min: 2, max: 80)]
    private ?string $name;

    #[ORM\Column(name: 'description', type: 'text', nullable: true)]
    #[Assert\Type(type: ['string', 'null'])]
    private ?string $description;

    #[ORM\Column(name: 'available', type: 'boolean', options: ['default' => 0])]
    #[Assert\Type(type: 'bool')]
    private bool $available = false;

    #[ORM\Column(name: 'price_per_minute', type: 'money')]
    /**
     * @Assert\Sequentially({
     *     @Assert\NotBlank(),
     *     @AppAssert\CurrencyConstraint(),
     *     @AppAssert\MoneyValuePositiveConstraint()
     * })
     */
    /**
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="object",
     *             "example"={
     *                 "amount": "120",
     *                 "currency": "EUR"
     *             }
     *         }
     *     }
     * )
     */
    private Money $pricePerMinute;

    #[ORM\Column(name: 'locales', type: 'simple_array', nullable: false)]
    /**
     * @Assert\All({
     *     @Assert\NotBlank,
     *     @Assert\Language,
     * })
     */
    /**
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array",
     *             "example"={
     *                 "en",
     *                 "fr"
     *             }
     *         }
     *     }
     * )
     */
    private array $locales = [];

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->pricePerMinute = Money::EUR(0);
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getPricePerMinute(): Money
    {
        return $this->pricePerMinute;
    }

    public function setPricePerMinute(Money $pricePerMinute): self
    {
        $this->pricePerMinute = $pricePerMinute;

        return $this;
    }

    public function hasLocale(string $locale): bool
    {
        return in_array($locale, $this->locales, true);
    }

    public function addLocale(string $locale): self
    {
        if (!$this->hasLocale($locale)) {
            $this->locales[] = $locale;
        }

        return $this;
    }

    public function removeLocale(string $locale): self
    {
        if ($this->hasLocale($locale)) {
            $this->locales = array_diff($this->locales, [$locale]);
        }

        return $this;
    }

    public function getLocales(): array
    {
        return $this->locales;
    }
}
