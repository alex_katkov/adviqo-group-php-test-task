FROM php:8-fpm-alpine

ARG UID=1000
ARG GID=1000
ARG ENABLE_XDEBUG=${ENABLE_XDEBUG}
ENV UID=${UID}
ENV GID=${GID}
ENV USER=a1ex-k
ENV HOME_DIR=/home/$USER
ENV COMPOSER_HOME=$HOME_DIR/.composer
ENV WAIT_VERSION 2.7.2

# Additional packages installing -------------------
RUN apk add wget bash git openssh

# Add docker-compose-wait tool -------------------
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

RUN install-php-extensions \
    bcmath \
    http \
    pdo_pgsql

# Xdebug check and install -------------------
RUN if [ "$ENABLE_XDEBUG" = "false" ] ; then echo 'Xdebug disabled'; else install-php-extensions xdebug; fi

# Add user and group -------------------
RUN addgroup -g $GID $USER \
    && adduser -u $UID -G $USER -s /bin/sh -D $USER

# Composer install -------------------
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer --quiet
RUN rm composer-setup.php

RUN chown -R $USER:$USER $HOME_DIR

COPY . /var/www/adviqo.app

RUN chown -R $USER:$USER /var/www/adviqo.app

WORKDIR /var/www/adviqo.app
