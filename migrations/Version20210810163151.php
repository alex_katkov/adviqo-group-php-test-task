<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210810163151 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE advisor (id UUID NOT NULL, name VARCHAR(80) NOT NULL, description TEXT DEFAULT NULL, available BOOLEAN DEFAULT \'false\' NOT NULL, price_per_minute VARCHAR(255) NOT NULL, locales TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN advisor.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN advisor.price_per_minute IS \'(DC2Type:money)\'');
        $this->addSql('COMMENT ON COLUMN advisor.locales IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE advisor');
    }
}
