#!/usr/bin/env bash

bin/console doctrine:migrations:migrate -n --allow-no-migration

if [ "$APP_ENV" != "production" ]; then
  bin/console doctrine:fixtures:load -q
fi
