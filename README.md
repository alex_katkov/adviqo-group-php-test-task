## Adviqo Group PHP Test Task

### Heroku
Project is hosted on **heroku**: https://adviqo-test.herokuapp.com/api

Also it's address of Swagger UI.

### Repository
Project Repository (GitLab): https://gitlab.com/alex_katkov/adviqo-group-php-test-task

### Local Installation
Project uses Docker for local environment and should be compatible with 
any popular Linux distribution.
Current project is not tested on MacOS, but usually this config works smoothly in most of the cases.
Also you should use `Maker` for better experience.

All build and prepare operations are done with `make install` command.
It builds PHP image, and starts local server.

You can start and stop server with corresponding commands: 
`make up` and `make stop`.

Dummy data can be generated with `make app-fixtures-load` command.
This will create 10 dummy records in `advisors` table.

### Code Quality
To ensure basic code quality we're using these tools:
1. `phpstan/phpstan`
2. `squizlabs/php_codesniffer`
3. `php-parallel-lint/php-parallel-lint`

Before pushing code to repository local checks can be run with
`make app-lint` command.

### Testing
PhpUnit tests are started with `make app-test` command.
Also, PhpUnit is used to run basic tests on API endpoints.
There is possibility to create DB fixtures for tests, extending `App\Tests\AliceFixtureDependentTestCase`.

These tests are not sufficient though and require too much effort to create for a long run.
#### TODO:
Replace with full endpoints coverage using [Behat](https://docs.behat.org/en/latest/) in future.

### About the State of The Test Task in General
1. CRUD is basically done
2. Order by Price is not done

*As I' using MoneyPhp lib to work with money we have a problem with sorting for now.
Basically if we assume that we have only one currency, than we can
fallback to regular scalar types and sort easily.*

*But having multiple currencies we have to determine some **user currency**
which must come from UserSettings and having some service's for conversion and converison rates.*

3. App is hosted on Heroku
4. GitLab CI is used for deployment (merge into main).
5. Gitlab CI tests every PR with "Code Quality" libs listed above and
runs PhpUnit tests before allowing to merge.

### Api
Api docs can be found at `/api` endpoint.

Example of filter/order enpoint: 

`/api/advisors?locales=en&&order[available]=desc`

##### Payload/Response example:
```json
{
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "name": "string",
  "description": "string",
  "available": true,
  "pricePerMinute": {
    "amount": "120",
    "currency": "EUR"
  },
  "locales": [
    "en",
    "fr"
  ]
}
```

### The thing:
One of the things which is currently in non satisfactory state is that `monyphp`
do not support decimal values out of the box, so right now it's possitble on to set integer values of price.
For this particular, considering integration with `APiPlatform` it can be done with 2 DTOs (input, output) 
and 2 transformers input/output.

The obvious minus of such a way is that it do not scales at all,
because for each entity where we have money.

** Another possiblity which will probably scale better is to create separate `Price` entity
and link it with `OneToOne` relationship to whenever we want.
This will allow us to have 2 DTO's and 2 Transformers for all use cases. 